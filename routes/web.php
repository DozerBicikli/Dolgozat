<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('');


Route::get('/students/show_deleted', [App\Http\Controllers\StudentController::class, 'show_deleted'])->name('students.show_deleted');
Route::put('/students/restore/{student}', [App\Http\Controllers\StudentController::class, 'restore'])->name('students.restore')->withTrashed();
Route::resource('/students', App\Http\Controllers\StudentController::class);


Route::resource('/courses', App\Http\Controllers\CourseController::class);

Route::get('/attendances/show_deleted', [App\Http\Controllers\AttendanceController::class, 'show_deleted'])->name('attendances.show_deleted');
Route::put('/attendances/restore/{attendances}',[App\Http\Controllers\AttendanceController::class, 'restore'])->name('attendances.restore')->withTrashed();
Route::resource('/attendances', App\Http\Controllers\AttendanceController::class);



Route::get('/learndays/show_deleted', [App\Http\Controllers\LearnDayController::class, 'show_deleted'])->name('learndays.show_deleted');
Route::put('/restore_learndays/{learndays}', [App\Http\Controllers\LearnDayController::class, 'restore'])->name('learndays.restore')->withTrashed();
Route::resource('/learndays', App\Http\Controllers\LearnDayController::class);