@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col"></div>
    <div class="col">
        <div class="card border-secondary bg-dark">
            <div class="card-body text-white">
                <h4 class="card-title">{{$student->name}}</h4>
                <p>Name of the Student: {{$student->name}}</p>
                <p>Birth of the Student: {{$student->birthDay}}</p>
                <p>Class of the Student: {{$student->course->name}}</p>    
                
                
            </div>
        </div>
    </div>
    <div class="col"></div>
</div>

@endsection