@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col"></div>
        <div class="col">
            <div class="card border-secondary bg-dark">
                <div class="card-body text-white">
                    <h4 class="card-title">Edit a Student named: {{ $student->name }}</h4>
                    <p class="card-text text-danger">Inputs marked with * shall be filled.</p>

                    @if ($errors->any())
                        <div class="mb-3 mt-3">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                                <strong>Holy guacamole!</strong>

                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                    @if (Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                            <strong>Holy guacamole!</strong>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif

                    <form action="{{ route('students.update', $student) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" name="name" id="name"
                                class="form-control @if ($errors->has('name')) border border-danger @endif"
                                placeholder="Student Name" aria-describedby="helpId"
                                value="{{ old('name', $student->name) }}">

                            @if ($errors->has('name'))
                                <small class="text-danger">{{ $errors->first('name') }}</small>
                            @else
                                <small id="helpId" class="text-white">The name of the course to be created.</small>
                            @endif

                        </div>
                        <div class="mb-3">
                            <label for="birthDay" class="form-label">Birth Date</label>
                            <input type="text" name="birthDay" id="birthDay" class="form-control"
                                placeholder="0000-00-00" aria-describedby="helpId" maxlength="255"
                                value="{{ old('description', $student->birthDay) }}">
                            <small id="helpId" class="text-white">Birth Date of the Student</small>
                        </div>
                        <div class="mb-3">
                            <label for="course" class="form-label">Class</label>
                            <select class="form-select" aria-label="Default select example" name="course_id">
                                <option hidden>Classes</option>
                                @foreach ($courses as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            <small id="helpId" class="text-white">Class of the Student</small>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>

@endsection
