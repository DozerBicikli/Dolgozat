@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col"></div>
    <div class="col">
        <div class="card border-secondary bg-dark">
            <div class="card-body text-white">
                <h4 class="card-title">{{$learndays->title}}</h4>
                <p>Name of the Student: {{$learndays->title}}</p>
                <p>Birth of the Student: {{$learndays->date}}</p>
                <p>Class of the Student: {{$learndays->course->name}}</p>    
                
                
            </div>
        </div>
    </div>
    <div class="col"></div>
</div>

@endsection