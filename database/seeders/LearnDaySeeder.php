<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LearnDaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\LearnDay::factory()->create([
            'title' => 'Hétfő',
            'date' => '2024-01-22'
        ]);
        \App\Models\LearnDay::factory()->create([
            'title' => 'Kedd',
            'date' => '2024-01-23'
        ]);
        \App\Models\LearnDay::factory()->create([
            'title' => 'Szerda',
            'date' => '2024-01-24'
        ]);
        \App\Models\LearnDay::factory()->create([
            'title' => 'Csütörtök',
            'date' => '2024-01-25'
        ]);

    }
}
