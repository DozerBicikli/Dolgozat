<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AttendanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Attendance::factory(32)->create([
            'learn_days_id' => '1',
        ]);

        \App\Models\Attendance::factory(32)->create([
            'learn_days_id' => '2',
        ]);
        \App\Models\Attendance::factory(32)->create([
            'learn_days_id' => '3',
        ]);
        \App\Models\Attendance::factory(32)->create([
            'learn_days_id' => '4',
        ]);
    }
}
