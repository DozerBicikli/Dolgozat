<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Student::factory()->create([
            'name' => 'Példa Béla',
            'birthDay' => '2004-04-04'
        ]);

        \App\Models\Student::factory(31)->create();

    }
}
