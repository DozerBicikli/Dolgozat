<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Attendance>
 */
class AttendanceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        static $id = 1;
        if ($id > 32) {
            $id = 1;
        }
        
        return [
            'status' => fake()->randomElement(['jelen', 'hiányzó', 'keresőképtelen']),
            'student_id' => $id++,
            'learn_days_id' => 1
        ];
        
    }
}
