<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attendance extends Model
{
    use HasFactory;
    /* use SoftDeletes; */


    protected $fillable = [
        'status',
        'learn_days_id',
        'student_id'
    ];

    
    public function student():BelongsTo
    {
        return $this->belongsTo(Student::class);
    }
    

    public function learnday(): BelongsTo
    {
        return $this->belongsTo(LearnDay::class, 'learn_days_id', 'id');
    }
}
