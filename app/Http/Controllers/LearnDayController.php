<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLearnDayRequest;
use App\Http\Requests\UpdateLearnDayRequest;
use App\Models\Course;
use App\Models\LearnDay;
use App\Models\Student;

class LearnDayController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $learndays = LearnDay::all();
        return view("learndays.index", ['learndays' => $learndays]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(LearnDay $learnday)
    {
        $courses = Course::all();
        return view('learndays.create' , ['learndays' => $learnday, 'courses' => $courses]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLearnDayRequest $request)
    {
        $learnday = new LearnDay(
            [
                "title" => $request->title,
                "date" => $request->date,
                "course_id" => $request->course_id
            ]
        );

        $learnday->save();

        return redirect()->route('attendances.store', $learnday);
    }

    /**
     * Display the specified resource.
     */
    public function show( $learnDay)
    {
        $learnDay = LearnDay::find($learnDay);
        return view('learndays.show', ['learndays' => $learnDay ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($learnDay)
    {
        $courses = Course::all();

        $learnDay = LearnDay::find($learnDay);
        return view('learndays.edit', ['learndays' => $learnDay, 'courses' => $courses]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLearnDayRequest $request, LearnDay $learnday)
    {
        
        $learnday->update($request->all());

        return redirect()->route('learndays.index')->with('success', 'The learn day was updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy( $learnDay)
    {

        $learnDay = LearnDay::find($learnDay);
        $learnDay->delete();
        return back()->with('success', 'Successfully deleted the learn day: ' . $learnDay->title . '.');
    }


    public function show_deleted()
    {
        $deleted_days = LearnDay::onlyTrashed()->get();
        return view('learndays.show_deleted', ['learndays' => $deleted_days]);
    }

    public function restore($learnDay)
    {
        $learnDay = LearnDay::withTrashed()->find($learnDay);
        $learnDay->restore();
        return back()->with('success', '' . $learnDay->title . ' was successfully restored.');
    }

}
